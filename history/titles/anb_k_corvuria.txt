k_corvuria = {
	1000.1.1 = { change_development_level = 8 }
	1008.6.1 = {	#End of Liberation of Busilar by Phoenix Empire
		liege = "e_bulwar"
		holder = sun_elvish0005 #Denarion Denarzuir
	}
}

d_bal_dostan = {
	1008.6.1 = {
		holder = sun_elvish0005 #Denarion Denarzuir
	}
}

c_arca_corvur = {
	1000.1.1 = { change_development_level = 10 }
}

c_corargin = {
	1000.1.1 = { change_development_level = 7 }
}

d_blackwoods = {
	1000.1.1 = { change_development_level = 7 }
	990.1.7 = {
		holder = 106 #Kolvan Karnid
	}
	1008.6.1 = {
		liege = "k_corvuria"
	}
}

c_holstead = {
	470.10.1 = {
	liege = d_blackwoods
	}
	952.8.9 = {
		holder = korbarid_0013 #Moskon Aldanid
	}
	998.4.3 = {
	holder = korbarid_0015 #Gebucu Aldanid
	}
	998.9.20 = {
	holder = korbarid_0008 #Bogan Aldanid
	}
}

c_kortir = {
	470.10.1 = {
	liege = d_blackwoods
	}
	941.12.12 = {
		holder = korbarid_0023 #Burebistan Buzasnid
	}
	988.8.8 = {
	holder = korbarid_0018 #Dorpokis Buzasnid
	}
	1019.7.28 = {
	holder = korbarid_0016 #Carnabon Buzasnid
	}
}

c_karns_hold = {
	1000.1.1 = { change_development_level = 6 }
}

d_ravenhill = {
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
		liege = "k_corvuria"
	}
}

c_ravenhill = {
	1000.1.1 = { change_development_level = 9 }
	
	1021.1.1 = {
		holder = 120 #Vasile sil Fiachlar
	}
}

c_gablaine = {
	1000.10.1 = {
		liege = d_ravenhill
	}
	1021.5.5 = {
		holder = corvurian_0015 #Azilina síl Mendruta
	}
}

c_arca_kaldere = {
	1000.10.1 = {
		liege = d_ravenhill
	}
	1021.5.5 = {
		holder = corvurian_0015 #Azilina síl Mendruta
	}
}

d_tiferben = {
	997.8.14 = {
		holder = 114 #Petre Tiferben
	}
	1008.6.1 = {
		liege = "k_corvuria"
	}
}

c_ebrosfeld = {
	470.10.1 = {
	liege = d_tiferben
	}
	986.1.1 = {
		holder = corvurian_0010 #Ceciliu Gahtalden
	}
	1012.2.6 = {
		holder =corvurian_0008 #Alveru Gahtalden
	}
}

c_gerbuta = {
	1000.1.1 = { change_development_level = 9 }
}

c_rotwall = {
	1000.1.1 = { change_development_level = 7 }
	
	470.10.1 = {
	liege = d_tiferben
	}
	986.1.1 = {
		holder = corvurian_0010 #Ceciliu Gahtalden
	}
	1012.2.6 = {
		holder =corvurian_0008 #Alveru Gahtalden
	}
}

c_ioans_ford = {
	470.10.1 = {
		liege = d_tiferben
	}
	1013.1.1 = {
		holder = corvurian_0014 #Tisiru Zenanid
	}
}

c_rackmans_court = {
	470.10.1 = {
		liege = d_tiferben
	}
	1013.1.1 = {
		holder = corvurian_0014 #Tisiru Zenanid
	}
}