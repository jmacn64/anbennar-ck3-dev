k_dameria = {
	1000.1.1 = { change_development_level = 8 }

	# This should be done as a realm law rather than a title law
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	
	757.3.20 = {
		holder = rubentis_0012 # Ruben V "the Wise" Rubentis
	}
	770.7.31 = {
		holder = rubentis_0013 # Lorenan II Rubentis
	}
	783.10.15 = {
		holder = rubentis_0018 # Ruben VI Rubentis
	}
	801.3.18 = {
		holder = rubentis_0019 # Emmeran III Rubentis
	}
	831.9.12 = {
		holder = rubentis_0020 # Ruben VII Rubentis
	}
	861.12.7 = {
		holder = rubentis_0022 # Lorenan III Rubentis
	}
	897.8.30 = {
		holder = dameris_0005 # Canrec "Moonborn" Dameris
	}
	915.10.30 = {
		holder = dameris_0006 # Aucanus Dameris
	}
	940.2.2 = {
		holder = dameris_0007 # Arrel Dameris
	}
	955.10.2 = {
		holder = dameris_0011 # Galien Dameris
	}
	980.12.1 = {
		holder = dameris_0012 # Auci Dameris
	}
	1021.10.3 = {
		holder = silmuna_0001 # Marion Silmuna
	}
}

d_damesear = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_dameria"
	}
	776.2.18 = {
		holder = rubentis_0013 # Lorenan II Rubentis
	}
	783.10.15 = {
		holder = rubentis_0018 # Ruben VI Rubentis
	}
	801.3.18 = {
		holder = rubentis_0019 # Emmeran III Rubentis
	}
	831.9.12 = {
		holder = rubentis_0020 # Ruben VII Rubentis
	}
	861.12.7 = {
		holder = rubentis_0022 # Lorenan III Rubentis
	}
	897.8.30 = {
		holder = dameris_0005 # Canrec "Moonborn" Dameris
	}
	915.10.30 = {
		holder = dameris_0006 # Aucanus Dameris
	}
	940.2.2 = {
		holder = dameris_0007 # Arrel Dameris
	}
	955.10.2 = {
		holder = dameris_0011 # Galien Dameris
	}
	980.12.1 = {
		holder = dameris_0012 # Auci Dameris
	}
	1021.10.3 = {
		holder = silmuna_0001 # Marion Silmuna
	}
}

c_varivar = {
	1000.1.1 = { change_development_level = 9 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1003.7.3 = {
		holder = 518 # Varilor Bluetongue
		effect = {
			add_quest_to_county = {
				QUEST = goblins_infestation
			}
		}
	}
}

c_eargate = {
	800.1.1 = {
		liege = "k_dameria"
	}
	1021.10.3 = {
		holder = 518 #Varilor Bluetongue
		government = feudal_government
		succession_laws = { elven_elective_succession_law elf_only }
	}
}

c_anbenncost = {
	1000.1.1 = { change_development_level = 10 }
	776.2.18 = {
		holder = dameris_0001 # Rogec Dameris
		liege = k_dameria
	}
	810.10.24 = {
		holder = dameris_0002 # Lorran Dameris
	}
	840.5.12 = {
		holder = dameris_0003 # Arrel Dameris
	}
	870.10.26 = {
		holder = dameris_0004 # Aucanus Dameris
	}
	880.10.3 = {
		holder = dameris_0005 # Canrec "Moonborn" Dameris
	}
	915.10.30 = {
		holder = dameris_0006 # Aucanus Dameris
	}
	940.2.2 = {
		holder = dameris_0007 # Arrel Dameris
	}
	955.10.2 = {
		holder = dameris_0011 # Galien Dameris
	}
	980.12.1 = {
		holder = dameris_0012 # Auci Dameris
	}
	1021.10.3 = {
		holder = silmuna_0001 # Marion Silmuna
		effect = {
			add_quest_to_county = {
				QUEST = goblins_infestation
			}
		}
	}
}

c_old_damenath = {
	1000.1.1 = { change_development_level = 13 }
	1021.10.3 = {
		holder = silmuna_0001
	}
}

c_damesteeth = {
	1000.1.1 = { change_development_level = 6 }
	979.6.6 = {
		holder = dunteris_0005
	}
	1013.10.3 = {
		holder = dunteris_0001
		liege = k_dameria
	}
}

c_moonmount = {
	1000.1.1 = { change_development_level = 7 }
	980.12.1 = {
		holder = dameris_0012 # Auci
	}
	1021.10.3 = {
		liege = k_dameria
		holder = damerian_0001 # High Priestess Aria of the Highest Moon
	}
}

c_auraire = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.3 = {
		holder = silmuna_0001
	}
}

d_wesdam = {
	898.11.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

c_wesdam = {
	898.11.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1000.1.1 = { change_development_level = 10 }
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

c_lenceiande = {
	898.11.2 = {
		liege = "k_dameria"
		holder = damerian7005 # William I
	}
	915.12.20 = {
		liege = "k_dameria"
		holder = damerian7006 # Caylen I
	}
	934.2.23 = {
		liege = "k_dameria"
		holder = damerian7007 # William II
	}
	945.3.26 = {
		liege = "k_dameria"
		holder = damerian7008 # Devan I
	}
	974.3.25 = {
		liege = "k_dameria"
		holder = damerian7009 # William III
	}
	990.11.25 = {
		liege = "k_dameria"
		holder = 7003 # Ricard I
	}
	1000.1.1 = { change_development_level = 10 }
	1014.7.24 = {
		liege = "k_dameria"
		holder = 7000 # Westyn I
	}
	1018.4.13 = {
		liege = "k_dameria"
		holder = 7002 # Westyn II
	}
}

d_exwes = {
	897.8.30 = {
		liege = k_dameria
	}
	959.6.23 = {
		holder = exwes_0001 # Dominic of Exwes
	}
	994.2.9 = {
		holder = exwes_0002 # William of Exwes
	}
}

d_istralore = {
	1000.1.1 = { change_development_level = 8 }
	800.1.1 = {
		liege = "k_dameria"
	}
	
	1021.1.4 = {
		holder = 19 #Istralania Warsinger
	}
}

c_istralore = {
	1000.1.1 = { change_development_level = 9 }
	800.1.1 = {
		liege = "d_istralore"
	}

	1021.1.4 = {
		holder = 19 #Istralania Warsinger
	}
}

c_damesdale = {
	1000.1.1 = { change_development_level = 8 }
}

c_ricancas = {
	1000.1.1 = { change_development_level = 8 }
	800.1.1 = {
		liege = "d_istralore"
	}

	1021.1.4 = {
		holder = 37 #Sideric
	}
}


d_neckcliffe = {
	1000.1.1 = { change_development_level = 8 }
	900.1.1 = {
		liege = "k_dameria"
	}
	
	
	950.1.1 = { # placeholder
		holder = cliffman_0001
	}
	986.1.7 = {
		holder = cliffman_0002
	}
	1013.10.3 = { # Battle of Damesteeth
		holder = cliffman_0003
	}
}

c_neckcliffe = {
	1000.1.1 = { change_development_level = 12 }
}

c_triancost = {
	1000.1.1 = { change_development_level = 9 }
}


d_acromton = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_dameria"
	}
	950.7.4 = {
		holder = acromis_0005
	}
	980.12.1 = {
		holder = acromis_0003
	}
	993.12.4 = {
		holder = acromis_0002 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = acromis_0001 #his successor, his brother, is not a loyalist
	}
}

c_acromton = {
	1000.1.1 = { change_development_level = 12 }
	800.1.1 = {
		liege = "d_acromton"
	}
	950.7.4 = {
		holder = acromis_0005
	}
	980.12.1 = {
		holder = acromis_0003
	}
	993.12.4 = {
		holder = acromis_0002 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = acromis_0001 #his successor, his brother, is not a loyalist
	}
}

c_damescross = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "d_acromton"
	}
	950.7.4 = {
		holder = acromis_0005
	}
	980.12.1 = {
		holder = acromis_0003
	}
	993.12.4 = {
		holder = acromis_0002 #Acromar, son of Acromar (500) was a damerian loyalist and killed by Sorcerer-King
	}
	999.7.4 = {	
		holder = acromis_0001 #his successor, his brother, is not a loyalist
	}
}


d_plumwall = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_dameria"
	}
	982.3.13 = {
		holder = prumuris_0002	#Betrayer Duke inherits from his father
	}
	1003.10.20 = {
		holder = prumuris_0001
	}
	1021.10.31 = {	
		holder = silmuna_0001	#Duchy and most of land was taken from betrayer duke's family in Treaty of Anbenncost
	}
}

c_plumwall = {
	1000.1.1 = { change_development_level = 12 }
	982.3.13 = {
		holder = prumuris_0002	#Betrayer Duke inherits from his father
	}
	1003.10.20 = {
		holder = prumuris_0001
		liege = "k_dameria"
	}
}

c_taxwick ={
	1000.1.1 = { change_development_level = 11 }
	982.3.13 = {
		holder = prumuris_0002	#Betrayer Duke inherits from his father
	}
	1003.10.20 = {
		holder = prumuris_0001
	}
	1021.10.31 = {
		holder = silmuna_0001
	}
}

c_silvelar = {
	1000.1.1 = { change_development_level = 11 }
	982.3.13 = {
		holder = prumuris_0002	#Betrayer Duke inherits from his father
	}
	1003.10.20 = {
		holder = prumuris_0001
	}
	1021.10.31 = {
		holder = silmuna_0001
	}
}


#d_upper_luna

c_cestaire = {
	1001.1.1 = { change_development_level = 12 }
	
	1003.12.8 = {
		holder = damerian7011
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_lancecastle = {
	1000.1.1 = { change_development_level = 9 }
	
	1003.12.8 = {
		holder = damerian7011
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_aranthil = {
	1000.1.1 = { change_development_level = 11 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1021.1.4 = {
		holder = 18 #Garion Silgarion
	}
}

c_westshields = {
	# 990.1.1 = {
		# holder = 0
	# }
	
	994.1.1 = {
		holder = vinerick_0001
	}
}


d_heartlands = {
	996.3.25 = {
		holder = losnaris_0001
	}
	1008.2.2 = {
		holder = 0
	}
}

c_dhaneir = {
	1000.1.1 = { change_development_level = 14 }
			1010.3.1 = {
		holder = damerian7030
	}
		1010.3.3 = {
		liege = "k_dameria"
	}
}


c_elensbridge = {
	1000.1.1 = { change_development_level = 12 }
	980.3.1 = {
		holder = damerian7033
	}
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_tirufeld = {
	1000.1.1 = { change_development_level = 10 }
	981.4.14 = {
		holder = natharis0001
	}
	1004.2.3 = {
		liege = "k_dameria"
	}
}

c_byshade = {
	1000.1.1 = { change_development_level = 9 }
	963.7.13 = {
		holder = losnaris_0006
	}
	984.6.7 = {
		holder = losnaris_0001
	}
	1008.2.2 = {
		holder = losnaris_0005
		liege = "k_dameria"
	}
}

c_palemarket = {
	1000.1.1 = { change_development_level = 11 }
	992.8.28 = {
		holder = laturis_0001
		liege = "k_dameria"
	}
}

c_eastbright = {
	1000.1.1 = { change_development_level = 10 }
	963.7.13 = {
		holder = losnaris_0006
	}
	984.6.7 = {
		holder = losnaris_0001
	}
	1008.2.2 = {
		holder = taoris_0001
		liege = k_dameria
	}
}

c_cannleigh = {
	1000.1.1 = { change_development_level = 8 }
	
	990.4.2 = {
		holder = damerian7016
	}
	
	1004.2.3 = {
		liege = "k_dameria"
	}
}

d_silverwoods = {
	1000.1.1 = { change_development_level = 6 }
	800.1.1 = {
		liege = "k_dameria"
	}
	1021.10.31 = {
		holder = 518 #Varilor Bluetongue
		government = feudal_government
		succession_laws = { elven_elective_succession_law elf_only }
	}
}

c_moonhaven = {
	1000.1.1 = { change_development_level = 8 }
}

c_dancers_retreat = {
	1021.10.31 = {
		holder = leafdancer_0003 #Oloris
		liege = "d_silverwoods"
	}
}
