﻿k_sarwick = {
	1000.1.1 = { change_development_level = 8 }
}

d_sarwood = {
	800.1.1 = {
		liege = "k_sarwick"
	}
	1021.9.23 = {
		holder = 96
	}
}

c_sarwick = {
	1021.9.23 = {
		holder = 96
	}
}

c_bogwood = {
	1021.9.23 = {
		holder = 96
	}
}

c_stenced = {
	1000.1.1 = { change_development_level = 7 }
	1021.9.23 = {
		holder = 96
	}
}

d_esshyl = {
	1007.8.23 = {
		holder = 101 #Lain Esshyl
	}
	1020.10.29 = {
		holder = 103 #Wynsten Edharlain
	}
}

c_esswyck = {
	1000.1.1 = { change_development_level = 10 }
	1007.8.23 = {
		holder = 101 #Lain Esshyl
	}
	1020.10.29 = {
		holder = 103 #Wynsten Edharlain
	}
}

c_esswood = {
	1007.8.23 = {
		holder = 101 #Lain Esshyl
	}
	1020.10.29 = {
		holder = 103 #Wynsten Edharlain
	}
}

c_farhyl = {
	1007.8.23 = {
		holder = 101 #Lain Esshyl
	}
	1020.10.29 = {
		holder = 103 #Wynsten Edharlain
	}
}

c_ordham = {
	1000.1.1 = { change_development_level = 7 }
}

c_watchers_wood = {
	1000.1.1 = { change_development_level = 7 }
}

c_kondunn = {
	1000.1.1 = { change_development_level = 7 }
}

c_fogwood = {
	1000.1.1 = { change_development_level = 7 }
}