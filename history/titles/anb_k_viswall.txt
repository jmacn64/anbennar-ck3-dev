k_viswall = {
	1000.1.1 = {
		change_development_level = 8
	}
	1021.10.3 = {
		holder = 26 #Finnic sil Vis
	}
}

c_viswall = {
	1000.1.1 = {
		change_development_level = 20
	}
}

c_norley = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_old_course = {
	1000.1.1 = {
		change_development_level = 7
	}
}

d_barrowshire = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_barrowshire = {
	1000.1.1 = {
		change_development_level = 11
	}
}

c_thomsbridge ={
	1000.1.1 = {
		change_development_level = 9
	}
}