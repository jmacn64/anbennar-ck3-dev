﻿
bulwari0001 = { # Husaam szal-Nisabat, Governor of Baharshes
	name = "Husaam"
	dynasty = dynasty_nisabat
	dna = 600_hussaam_nisabat
	religion = bulwari_sun_cult
	culture = bahari
	
	diplomacy = 10
	martial = 6
	stewardship = 9
	intrigue = 4
	learning = 10
	prowess = 5
	
	trait = race_human
	trait = education_diplomacy_3
	trait = zealous
	trait = humble
	trait = just
	
	980.5.11 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
}

bulwari0002 = { # Paramaš szal-Nisabat, heir of Baharshes
	name = "ParamaS_"
	dynasty = dynasty_nisabat
	religion = bulwari_sun_cult
	culture = bahari
	
	trait = race_human
	
	father = bulwari0001
	# mother = bulwari0003
	
	1014.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

# bulwari0003 = { # Wife of Husaam
# }

bulwari0004 = { # Bavar II szel-Barseen, Governor of Zansap and Sad Sur
	name = "Bavar"
	dynasty = dynasty_barseen
	religion = bulwari_sun_cult
	culture = barsibu
	
	trait = race_human
	# trait = education_intrigue_
	trait = zealous
	trait = ambitious
	trait = wrathful
	
	984.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

bulwari0006 = { # Asor szel-Brasan, Rabsukal of Brasan
	name = "Asor"
	dynasty = dynasty_brasan
	religion = bulwari_sun_cult
	culture = brasanni
	
	trait = race_human
	# trait = education_
	trait = zealous
	trait = patient
	trait = greedy
	
	960.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

bulwari0007 = { # Zimud Semiz, Akal of Azka-Sur
	name = "Zimud"
	dynasty = dynasty_semiz
	religion = "old_bulwari_sun_cult"
	culture = "surani"
	
	trait = race_human
	# trait = education_
	trait = cynical
	trait = arrogant
	trait = brave
	
	980.3.14 = {
		birth = yes
	}
}

# bulwari0008 = { # Wife of Zimud
# }

bulwari0009 = { # Benan Semiz, daughter of Zimud, Wife of Udeš of Hašr
	name = "Benan"
	dynasty = dynasty_semiz
	religion = "old_bulwari_sun_cult"
	culture = "surani"
	female = yes
	
	trait = race_human
	# trait = education_
	
	father = bulwari0007
	# mother = bulwari0008
	
	1000.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1017.1.1 = { # random date PLACEHOLDER
		add_spouse = bulwari0010 # Udeš of Hašr
	}
}

bulwari0010 = { # Udeš Ašrat, Akal of Hašr
	name = "UdeS_"
	dynasty = dynasty_asrat
	religion = "old_bulwari_sun_cult"
	culture = "surani"
	
	trait = race_human
	# trait = education_
	trait = cynical
	trait = stubborn
	trait = trusting
	
	999.4.12 = {
		birth = yes
	}
	
	1017.1.1 = { # random date PLACEHOLDER
		add_spouse = bulwari0009 # Udeš of Hašr
	}
}

bulwari0011 = { # Radin szel-Ĕrubas, Akal of Avamezan
	name = "Radin"
	dynasty = dynasty_erubas
	religion = "old_bulwari_sun_cult"
	culture = "akalsesi"
	
	trait = race_human
	# trait = education_
	
	992.7.25 = {
		birth = yes
	}
}

bulwari0012 = { # Naram szel-Gelkalis, Akal of Gelkalis
	name = "Naram"
	dynasty = dynasty_gelkalis
	religion = "old_bulwari_sun_cult"
	culture = "gelkar"
	
	trait = race_human
	# trait = education_
	
	984.7.29 = {
		birth = yes
	}
}

bulwari0013 = {
	name = "Dagan"
	dynasty = dynasty_akalses #szel-Akalšes
	religion = "old_bulwari_sun_cult"
	culture = "akalsesi"
	
	trait = race_human
	# trait = education_
	
	986.10.14 = {
		birth = yes
	}
}

#masnsih tribes bulwari1000-bulwari10100 for lowborn

adad_0001 = {
	name = "Darian"
	dynasty = dynasty_adad #szel-Adad
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_4
	trait = ambitious
	trait = diligent
	trait = impatient
	trait = strong
	trait = desert_warrior
	
	970.6.14 = {
		birth = yes
	}
	1021.4.5 = {
		death = yes
	}
}
adad_0002 = {
	name = "Arad"
	dynasty = dynasty_adad #szel-Adad
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_2
	trait = zealous
	trait = stubborn
	trait = patient
	trait = depressed_1
	
	father = adad_0001

	999.1.24 = {
		birth = yes
	}
	1021.4.5 = {
		add_spouse = zaid_0001
	}
}

adad_0003 = {
	name = "Dagan"
	dynasty = dynasty_adad #szel-Adad
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_intrigue_3
	trait = deceitful
	trait = stubborn
	trait = vengeful
	trait = strong
	
	father = adad_0001

	999.1.24 = {
		birth = yes
	}
	1021.4.5 = {
		add_spouse = zaid_0001
	}
}

zaid_0001 = {
	name = "Haamida"
	dynasty = dynasty_zaid
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_intrigue_2
	trait = lustful
	trait = generous
	trait = deceitful
	
	female = yes

	father = zaid_0002	
	mother = zaid_0003

	1001.5.21 = {
		birth = yes
	}
}

zaid_0002 = {
	name = "ArS_am"
	dynasty = dynasty_zaid
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_intrigue_3
	trait = zealous
	trait = brave
	trait = impatient
	trait = journaller
	
	965.7.11 = {
		birth = yes
	}
	997.3.11 = {
		add_spouse = zaid_0003
	}
}

zaid_0003 = {
	name = "Yamina"
	dynasty = dynasty_bulati
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_diplomacy_2
	trait = patient
	trait = arrogant
	trait = calm
	trait = comfort_eater
	
	female = yes

	979.2.11 = {
		birth = yes
	}
}

zaid_0004 = {
	name = "Dartaxes"
	dynasty = dynasty_zaid
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_3
	trait = wrathful
	trait = brave
	trait = gluttonous
	trait = comfort_eater
	trait = holy_warrior
	
	father = zaid_0002
	mother = zaid_0003

	1003.9.21 = {
		birth = yes
	}
}

zaid_0005 = {
	name = "Mesten"
	dynasty = dynasty_zaid
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_4
	trait = sadistic
	trait = patient
	trait = trusting
	
	father = zaid_0002
	mother = zaid_0003

	1003.9.21 = {
		birth = yes
	}
}

bulwari_1001 = {
	name = "Ettu"
	#dynasty = 
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_diplomacy_2
	trait = lustful
	trait = forgiving
	trait = generous
	
	female = yes

	980.4.12 = {
		birth = yes
	}
}

zaid_0006 = {
	name = "Amina"
	dynasty = dynasty_zaid
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	female = yes
	
	trait = race_human
	trait = education_diplomacy_3
	trait = fickle
	trait = lustful
	trait = calm
	
	father = zaid_0002
	mother = bulwari_1001

	1001.6.21 = {
		birth = yes
	}
}

yazkur_0001 = {
	name = "Yazkur"
	dynasty = dynasty_yazkur #szel-Yazkur
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_4
	trait = brave
	trait = ambitious
	trait = wrathful
	trait = peasant_leader
	trait = desert_warrior
	
	904.6.14 = {
		birth = yes
	}
	957.7.15 = {
		death = yes
	}
}

yazkur_0002 = {
	name = "Duzi"
	dynasty = dynasty_yazkur #szel-Yazkur
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_1
	trait = craven
	trait = fickle
	trait = paranoid
	
	father = yazkur_0001

	921.6.14 = {
		birth = yes
	}
	990.9.6 = {
		death = yes
	}
}

yazkur_0003 = {
	name = "Erol"
	dynasty = dynasty_yazkur #szel-Yazkur
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_4
	trait = vengeful
	trait = stubborn
	trait = zealous
	trait = desert_warrior
	trait = holy_warrior
	
	father = yazkur_0002

	980.8.15 = {
		birth = yes
	}
	1002.5.1 = {
		add_spouse = bulwari_1002
	}
}

bulwari_1002 = {
	name = "Gula"
	#dynasty
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_diplomacy_2
	trait = lazy
	trait = patient
	trait = fickle
	
	female = yes

	981.3.12 = {
		birth = yes
	}
}

yazkur_0004 = {
	name = "Tozan"
	dynasty = dynasty_yazkur #szel-Yazkur
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_martial_3
	trait = content
	trait = vengeful
	trait = wrathful
	trait = desert_warrior
	
	father = yazkur_0003
	mother = bulwari_1002

	1003.4.22 = {
		birth = yes
	}
}

ayarzil_0001 = {
	name = "Durul"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_learning_2
	trait = paranoid
	trait = content
	trait = arrogant

	947.2.24 = {
		birth = yes
	}
	1008.4.4 = {
		death = yes
	}
}

ayarzil_0002 = {
	name = "Kaveh"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_3
	trait = diligent
	trait = honest
	trait = greedy

	father = ayarzil_0001
	
	987.3.14 = {
		birth = yes
	}
	1011.5.8 = {
		add_spouse = attalu_0001
	}
	1020.2.7 = {
		add_spouse = zaid_0006
	}
}

ayarzil_0003 = {
	name = "Benan"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_2
	trait = stubborn
	trait = patient
	trait = greedy

	female = yes
	father = ayarzil_0001
	
	992.9.5 = {
		birth = yes
	}
}

ayarzil_0004 = {
	name = "Mehran"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_3
	trait = patient
	trait = gregarious
	trait = diligent

	father = ayarzil_0001
	
	1001.5.9 = {
		birth = yes
	}
}

attalu_0001 = {
	name = "Semiha"
	dynasty = dynasty_attalu #szel-Attlu
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_intrigue_3
	trait = paranoid
	trait = wrathful
	trait = arrogant

	female = yes
	father = attalu_0002
	
	992.6.3 = {
		birth = yes
	}
}

ayarzil_0005 = {
	name = "Abella"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = paranoid

	female = yes
	father = ayarzil_0002
	mother = attalu_0001
	
	1012.5.2 = {
		birth = yes
	}
}

ayarzil_0006 = {
	name = "Jannat"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human

	female = yes
	father = ayarzil_0002
	mother = attalu_0001
	
	1018.6.7 = {
		birth = yes
	}
}

ayarzil_0007 = {
	name = "Aloros"
	dynasty = dynasty_ayarzil #szel-Ayarzil
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human

	father = ayarzil_0002
	mother = attalu_0001
	
	1020.12.7 = {
		birth = yes
	}
}

attalu_0002 = {
	name = "Saamir"
	dynasty = dynasty_attalu #szel-Attalu
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = education_stewardship_3
	trait = lustful
	trait = humble
	trait = vengeful

	964.1.24 = {
		birth = yes
	}
	1006.2.8 = {
		death = yes
	}
}

attalu_0003 = {
	name = "Duzar"
	dynasty = dynasty_attalu #szel-Attalu
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = education_martial_4
	trait = race_human
	trait = diligent
	trait = gregarious
	trait = trusting
	trait = lifestyle_blademaster
	trait = adventurer

	father = attalu_0002
	
	988.2.9 = {
		birth = yes
	}
	1012.5.8 = {
		add_spouse = ayarzil_0003
		
		add_trait_xp = {
			trait = lifestyle_blademaster
			value = 50
		}
	}
}

attalu_0004 = {
	name = "Aloros"
	dynasty = dynasty_attalu #szel-Attalu
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = race_human
	trait = charming

	father = attalu_0003
	mother = ayarzil_0003
	
	1016.6.5 = {
		birth = yes
	}
}

ardeth_0001 = {
	name = "Darion"
	dynasty = dynasty_ardeth #szel-Ardeth
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = education_martial_4
	trait = race_human
	trait = shy
	trait = calm
	trait = paranoid
	trait = desert_warrior

	
	997.5.5 = {
		birth = yes
	}
}

surubaz_0001 = {
	name = "Samiun"
	dynasty = dynasty_surubaz #szel-Surubaz
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	
	trait = education_intrigue_2
	trait = race_human
	trait = callous
	trait = arbitrary
	trait = arrogant
	
	998.6.18 = {
		birth = yes
	}
}

bulati_0001 = {
	name = "Adenn"
	dynasty = dynasty_bulati #szel-Bulat
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}

selabis_0001 = {
	name = "Demartos"
	dynasty = dynasty_selabis #szel-Šelabis
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}

rijascar_0001 = {
	name = "Darab"
	dynasty = dynasty_rijascar #szel-Rijašcar
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}

susi_0001 = {
	name = "Sharaf"
	dynasty = dynasty_susi #szel-Suši
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}

aruru_0001 = {
	name = "Huran"
	dynasty = dynasty_aruru #szel-Aruru
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}

betikalbu_0001 = {
	name = "Rimush"
	dynasty = dynasty_betikalbu #szel-Betikalbu
	religion = "old_bulwari_sun_cult"
	culture = "masnsih"
	990.1.1 = {
		birth = yes
	}
}
