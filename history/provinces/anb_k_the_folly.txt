#k_the_folly
##d_nathalaire
###c_nathalaire
451 = {		#Nathalaire

    # Misc
    culture = west_divenori
    religion = tefori_damish
	holding = castle_holding

    # History
}

6385 = {		#

    # Misc
    culture = west_divenori
    religion = tefori_damish
	holding = city_holding

    # History
}

6386 = {		#

    # Misc
    culture = west_divenori
    religion = tefori_damish
	holding = none

    # History
}

6387 = {		#

    # Misc
    culture = west_divenori
    religion = tefori_damish
	holding = none

    # History
}

###c_corveld_coast
445 = {     #Jorathur

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = castle_holding

    # History
}

6376 = {

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = none

    # History
}

6377 = {    #Corveld

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = none

    # History
}

###c_448_test
448 = {

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = castle_holding

    # History
}

449 = {

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = none

    # History
}

##d_daravan_folly
###c_dreadmire
6382 = {    #Dreadvord

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = none

    # History
}

446 = {     #Dreadmire

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = castle_holding

    # History
}

###c_nathfort
444 = {     #Nathfort

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = castle_holding

    # History
}

443 = {     #

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = none

    # History
}

###c_xhazobain_end
442 = {     #Xhazobain's End

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = castle_holding

    # History
} 

6383 = {     #Bronhyl

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = none

    # History
}

6384 = {     #

    # Misc
    culture = corvurian
    religion = castanorian_pantheon
    holding = none

    # History
}

###c_lorest_watch
450 = {     #Lorest' Watch

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = castle_holding

    # History
} 

6380 = {     #

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = none

    # History
}

6381 = {     #

    # Misc
    culture = west_divenori
    religion = tefori_damish
    holding = none

    # History
}
