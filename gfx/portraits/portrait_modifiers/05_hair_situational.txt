﻿# Anbennar: commenting vanilla stuff, non-human male pattern baldness corrections

hair_situational = {
	usage = game
	priority = 10
	selection_behavior = max

	no_hair = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = hairstyles
				template = no_hairstyles
				value = 0
			}
		}   
		outfit_tags = { bald }
		weight = {
			base = 0
			# modifier = { # Anbennar
				# add = 1000
                # exists = this
			   	# has_character_modifier = hajj_halq_modifier
			# }
		}
	}

	no_baldness = {
		dna_modifiers = {
			morph = {
				mode = add
				gene = gene_balding_hair_effect
				template = no_baldness
				value = 1
			}		
		} 
		weight = {
			base = 50
			modifier = {
				add = 200
				scope:age < 25 
			}
			modifier = {
				factor = 0
				portrait_steppe_clothing_trigger = yes
			}
		}
	}

	shaved_baldness = {
		dna_modifiers = {
			morph = {
				mode = add
				gene = gene_balding_hair_effect
				template = shaved_baldness
				value = 1
			}		
		} 
		weight = {
			base = 50
			modifier = {
				add = 200
				scope:age < 25 
			}
			modifier = {
				factor = 0
				NOT = {
					portrait_steppe_clothing_trigger = yes
				}
			}
			# Anbennar
			modifier = { # Elves don't grow beards
				factor = 1000
				OR = {
					has_trait = race_elf
					has_trait = race_elf_dead
				}
			}
		}
	}

	male_pattern_baldness_stage_1 = { 
		dna_modifiers = {
			morph = {
				mode = add
				gene = gene_balding_hair_effect
				template = baldness_stage_1
				value = 1
			}
		} 
		weight = {
			base = 100
			modifier = { 
				factor = {
					value = 0
				}
				trigger = {
					morph_gene_value:gene_baldness > {
						value = age
						multiply = 0.01
					}
				}
			}
			modifier = {
				factor = 0
				is_female = yes
			}	
			# Anbennar
			modifier = {
				factor = 0
				OR = {
					has_trait = race_elf
					has_trait = race_elf_dead
					has_trait = race_half_elf
					has_trait = race_dwarf
				}
			}
		}		  	
	}

	male_pattern_baldness_stage_2 = { 
		dna_modifiers = {
			morph = {
				mode = add
				gene = gene_balding_hair_effect
				template = baldness_stage_2
				value = 1
			}
		} 
		weight = {
			base = 150
			modifier = { 
				factor = {
					value = 0
				}
				trigger = {
					morph_gene_value:gene_baldness > {
						value = age
						multiply = 0.005
					}
				}
			}	
			modifier = {
				factor = 0
				is_female = yes
			}
			# Anbennar
			modifier = {
				factor = 0
				OR = {
					has_trait = race_elf
					has_trait = race_elf_dead
					has_trait = race_half_elf
					has_trait = race_dwarf
				}
			}
		}		  	
	}
	
}

