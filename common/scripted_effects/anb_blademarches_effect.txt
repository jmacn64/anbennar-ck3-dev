﻿blademarches_succession_determine_success = {
	if = {
		limit = {
			NAND = {
				has_character_modifier = wielded_calindal
				has_character_modifier = blinded_by_calindal
			}
		}
		custom_tooltip = blademarches_success_modifiers_tooltip
	}
	random_list = {
		5 = {
			# Add Prowess * 0.75 up to 20
			modifier = {
				add = this.prowess
				this.prowess <= 20
				factor = 0.75
			}
			modifier = {
				add = 15
				this.prowess > 20
			}
			# Add Martial * 0.5 up to 20
			modifier = {
				add = this.martial
				this.martial <= 20
				factor = 0.5
			}
			modifier = {
				add = 10
				this.martial > 20
			}
			# Good traits
			modifier = {
				factor = 1.5
				has_trait = just
			}
			modifier = {
				factor = 1.5
				has_trait = brave
			}
			# More likely if in Blademarches (any held title)
			modifier = {
				add = 5
				any_held_title = {
					any_this_title_or_de_jure_above = {
						this = title:k_blademarches
					}
				}
			}
			# Already blinded by calindal
			modifier = {
				factor = 0
				has_character_modifier = blinded_by_calindal
			}
			modifier = {
				factor = 0
				has_trait = blind
			}
			add_character_modifier = wielded_calindal
		}
		15 = {
			# Can't fail if you've already done it before
			modifier = {
				factor = 0
				has_character_modifier = wielded_calindal 
			}
			# Bad traits
			modifier = {
				factor = 2
				has_trait = arbitrary
			}
			modifier = {
				factor = 5
				has_trait = craven
			}
			add_character_modifier = blinded_by_calindal
			add_trait_force_tooltip = blind
		}
	}
	remove_character_modifier = untested_bladeking
}

# Goes to the next dude in line to get the blade
blademarches_succession_reject_effect = {
	if = {
		limit = {
			NOT = { has_character_modifier = blinded_by_calindal }
		}
		add_character_modifier = { modifier = refused_calindal years = 10 } # Cannot try again for 10 years
	}
	# Lose your claim to Blademarches if you don't take the trials
	if = {
		limit = { has_claim_on = title:k_blademarches  }
		remove_claim = title:k_blademarches
	}
	# Removed Bladeshunned if you reject Blademarches trial
	if = {
		limit = { has_trait = bladeshunned }
		remove_trait = bladeshunned
	}
	custom_tooltip = search_for_claimant_continues_tt
	# Vassals with a claim to blademarches take the blade test
	if = {
		limit = {
			title:k_blademarches = {
				any_de_jure_county_holder = {
					has_strong_claim_on = title:k_blademarches
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
			}
		}
		title:k_blademarches = {
			random_de_jure_county_holder = {
				limit = {
					has_strong_claim_on = title:k_blademarches
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
				trigger_event = { id = blademarches_succession.0001 days = 3 }
			}
		}
	}
	# Vassals with a weak claim to blademarches take the blade test
	else_if = {
		limit = {
			title:k_blademarches = {
				any_de_jure_county_holder = {
					has_weak_claim_on = title:k_blademarches
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
			}
		}
		title:k_blademarches = {
			random_de_jure_county_holder = {
				limit = {
					has_weak_claim_on = title:k_blademarches
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
				trigger_event = { id = blademarches_succession.0001 days = 3 }
			}
		}
	}
	# TODO - Bladesteward takes the test
	# Vassals take the blade
	else_if = {
		limit = {
			title:k_blademarches = {
				any_de_jure_county_holder = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
			}
		}
		title:k_blademarches = {
			random_de_jure_county_holder = {
				limit = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
				trigger_event = { id = blademarches_succession.0001 days = 3 }
			}
		}
	}
	# Random nieghbouring ruler with claims (ones who are nearby)
	else_if = {
		limit = {
			title:k_blademarches.holder = {
				any_neighboring_top_liege_realm_owner = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
					has_strong_claim_on = title:k_blademarches
				}
			}
		}
		title:k_blademarches.holder = {
			random_neighboring_top_liege_realm_owner = {
				limit = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
					has_strong_claim_on = title:k_blademarches
				}
				trigger_event = { id = blademarches_succession.0001 days = 10 }
			}
		}
	}
	else_if = {
		limit = {
			title:k_blademarches.holder = {
				any_neighboring_top_liege_realm_owner = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
					has_claim_on = title:k_blademarches
				}
			}
		}
		title:k_blademarches.holder = {
			random_neighboring_top_liege_realm_owner = {
				limit = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
					has_claim_on = title:k_blademarches
				}
				trigger_event = { id = blademarches_succession.0001 days = 10 }
			}
		}
	}
	# Random neighbouring ruler
	else_if = {
		limit = {
			title:k_blademarches.holder = {
				any_neighboring_top_liege_realm_owner = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
			}
		}
		title:k_blademarches.holder = {
			random_neighboring_top_liege_realm_owner = {
				limit = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
				trigger_event = { id = blademarches_succession.0001 days = 10 }
			}
		}
	}
	# Random claimants take the test
	else_if = {
		limit = {
			title:k_blademarches = {
				any_claimant = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
			}
		}
		title:k_blademarches = {
			random_claimant = {
				limit = {
					NOT = { has_character_modifier = refused_calindal }
					NOT = { has_character_modifier = blinded_by_calindal }
				}
				trigger_event = { id = blademarches_succession.0001 days = 10 }
			}
		}
	}
	# Just generate a dude to pass the blade test
	else = {
		create_character = {
			save_scope_as = calindal_wielder
			template = bladesteward_character
			dynasty = none
			location = root.capital_province
		}
		scope:calindal_wielder = {
			add_character_modifier = wielded_calindal
			add_prestige = medium_prestige_gain
		}
		trigger_event = { id = blademarches_succession.0001 days = 30 }
	}
}

establish_the_bladestewards_tooltip_effect = {
	# Create a new castle and move the capital there
	
	custom_tooltip = create_bladestewards_decision_effect_message
	
	hidden_effect = {
		if = {
			limit = {
				title:c_stewards_hold = {
					any_county_province = { has_holding = no }
				}
			}
			title:c_stewards_hold = {
				random_county_province = {
					limit = { has_holding = no }
					set_holding_type = castle_holding
					barony = { save_scope_as = leased_barony }
				}
			}
		}
		else = {
			title:c_stewards_hold = {
				random_county_province = {
					limit = {
						is_county_capital = no
						OR = {
							has_holding_type = castle_holding
							has_holding_type = city_holding
						}
					}
					barony = { save_scope_as = leased_barony }
					set_holding_type = castle_holding
				}
			}
		}
	}

	# Create a holy order
	hidden_effect = {
		if = {
			limit = { NOT = { scope:leased_barony.holder = root } }
			create_title_and_vassal_change = {
				type = leased_out
				save_scope_as = change
				add_claim_on_loss = no
			}
			scope:leased_barony = {
				change_title_holder_include_vassals = {
					holder = root
					change = scope:change
				}
			}
			resolve_title_and_vassal_change = scope:change
		}
	}

	create_character = {
		template = holy_order_leader_character
		location = title:b_stewards_hold.title_province
		save_scope_as = leader
	}
	
	# This just creates a random holy order
	create_holy_order = {
		leader = scope:leader
		capital = scope:leased_barony
		save_scope_as = new_holy_order
	}

	hidden_effect = {
		if = { # If you have no valid barony this will throw errors when you open the decision since the leader won't be saved - we add this check to avoid that 
			limit = {
				exists = scope:leader
			}
			scope:leader = {
				add_gold = 100 #So that they have some money to lend out
				add_piety_level = 2
				add_gold = holy_order_starting_gold
				every_courtier = {
					add_trait = order_member
				}
			}
			
			# Messages
			send_interface_toast = {
				type = holy_order_founded_message
				desc = i_created_holy_order_message
				left_icon = scope:leader
				right_icon = scope:new_holy_order.title
			}
		}
		
		set_global_variable = {
			name = bladestewards_title
			value = scope:leader.primary_title
		}
		
		scope:leader.primary_title = { set_coa = ho_bladestewards }
		
		scope:leased_barony.title_province = {
			# Walls & Towers.
			if = {
				limit = {
					NOT = { has_building_or_higher = curtain_walls_01 }
				}
				# If it doesn't have a free building slot, give it one.
				if = {
					limit = { free_building_slots = 0 }
					add_province_modifier = extra_building_slot
				}
				# Add the building.
				add_building = curtain_walls_01
			}
			# Barracks.
			if = {
				limit = {
					NOT = { has_building_or_higher = barracks_01 }
				}
				# If it doesn't have a free building slot, give it one.
				if = {
					limit = { free_building_slots = 0 }
					add_province_modifier = extra_building_slot
				}
				# Add the building.
				add_building = barracks_01
			}
			# Military Camps.
			if = {
				limit = {
					NOT = { has_building_or_higher = military_camps_01 }
				}
				# If it doesn't have a free building slot, give it one.
				if = {
					limit = { free_building_slots = 0 }
					add_province_modifier = extra_building_slot
				}
				# Add the building.
				add_building = military_camps_01
			}
		}
	}
}

have_bladesteward_wield_calindal_effect = {
	custom_tooltip = bladesteward_wield_calindal_effect
	
	if = {
		limit = { has_trait = bladeshunned }
		remove_trait = bladeshunned
	}
	
	hidden_effect = {
		create_character = {
			save_scope_as = calindal_wielder
			template = bladesteward_character
			dynasty = none
			location = root.capital_province
		}
		scope:calindal_wielder = {
			add_character_modifier = wielded_calindal
			add_unpressed_claim = title:k_blademarches
			add_prestige = medium_prestige_gain
			add_character_modifier = bladesteward
		}
		
		title:k_blademarches.holder = {
			trigger_event = { id = blademarches_succession.0004 }
		}
	}
}