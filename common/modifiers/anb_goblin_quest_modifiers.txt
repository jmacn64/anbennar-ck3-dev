﻿goblins_infestation_modifier = {
	icon = rat_negative
	
	tax_mult = -0.05
	development_growth = -0.05
}

returned_goblin_loot = {
	icon = stewardship_positive
	
	county_opinion_add = 5
	development_growth = 0.05
}

accepted_goblin_settlers = {
	icon = rat_positive
	tax_mult = 0.15
	county_opinion_add = -15
}

freed_goblin_slaves = {
	icon = diplomacy_positive
	
	diplomacy = 3
}

goblin_slave_labour = {
	icon = dread_positive
	
	tax_mult = 0.05
	build_speed = 0.1
	build_gold_cost = -0.05
	build_piety_cost = -0.05
	build_prestige_cost = -0.05
}

fortified_goblin_lair = {
	icon = county_modifier_development_negative

	levy_reinforcement_rate = -0.33
	fort_level = 1
}

claimed_goblin_fortifications = {
	icon = county_modifier_development_positive

	fort_level = 1
}