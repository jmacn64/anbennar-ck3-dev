﻿uelairey = {
	trigger = {
		scope:culture = { has_cultural_pillar = heritage_damesheader }
		OR = {
			capital_county = {
				title:k_uelaire = {
					is_de_jure_liege_or_above_target = prev
				}
			}
			has_primary_title = title:d_uelaire
			has_primary_title = title:k_uelaire
		}
	}
}

blue_reachman = {
	trigger = {
		OR = {
			culture = culture:gawedi
			culture = culture:old_alenic
		}
		OR = {
			capital_province = {
				geographical_region = custom_alenic_reach
			}

			liege = {
				culture = {
					has_cultural_pillar = language_reachman_common
				}
			}
		}
	}
}

snow_elvish = {
	trigger = {
		scope:culture = { has_cultural_pillar = heritage_elven }
		capital_province = {
			OR = {
				geographical_region = world_cannor_gerudia
				geographical_region = custom_alenic_reach		
			}
		}
	}
}